package com.spring.webflux.scheme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebfluxSchemeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebfluxSchemeApplication.class, args);
	}

}
