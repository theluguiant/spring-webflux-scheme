package com.spring.webflux.scheme.repository.mongoOne;

import com.spring.webflux.scheme.models.mongoOne.documents.Producto;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ProductRepository extends ReactiveMongoRepository<Producto, String> {
}
