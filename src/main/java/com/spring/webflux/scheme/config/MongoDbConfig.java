package com.spring.webflux.scheme.config;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@EnableReactiveMongoRepositories(basePackages="com.spring.webflux.scheme.repository.mongoOne")
public class MongoDbConfig extends AbstractReactiveMongoConfiguration {

    @Value("${mongoOne.database.url}")
    private String url;

    @Value("${mongoOne.database.name}")
    private String name;

    @Value("${mongoOne.database.port}")
    private String port;

    @Override
    protected String getDatabaseName() {
        return name;
    }

    @Override
    public MongoClient reactiveMongoClient() {
        return MongoClients.create(url+":"+port);
    }

}
